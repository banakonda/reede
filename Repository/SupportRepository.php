<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//SuppMess.php';
require_once __DIR__.'//..//Models//Support.php';

class SupportRepository extends Repository {
    // stworz watek [ID_watek	status	ID_author	date	title]
    public function makeWatek(string $status, int $ID_author, string $date, string $title){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO supp_watek ( status, ID_author, date, title) 
            VALUES (:status, :ID_author, :date, :title)
            ');
        $stmt->bindParam(':status', $status, PDO::PARAM_STR);
        $stmt->bindParam(':ID_author', $ID_author, PDO::PARAM_INT);
        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->execute();
    }  
    // wyslij wiadomosc [ID_message 	ID_watek	who	    date content]
    public function sendSuppMess(string $ID_watek, string $who, string $date, string $content){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO supp_message (ID_watek, who, date, content) 
            VALUES (:ID_watek, :who, :date, :content)
            ');
        $stmt->bindParam(':ID_watek', $ID_watek, PDO::PARAM_INT);
        $stmt->bindParam(':who', $who, PDO::PARAM_STR);
        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->bindParam(':content', $content, PDO::PARAM_STR);
        $stmt->execute();
    } 
    // Pobierz id ostateniego watku
    public function lastId() :int{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM supp_watek
            ORDER BY ID_watek desc
            LIMIT 1
            ');
        $stmt->execute();

        $one = $stmt->fetch(PDO::FETCH_ASSOC);

        $result = new Support(
            $one['ID_watek'],
                $one['status'],
                $one['ID_author'],
                $one['date'],
                $one['title']
        );

        return $result->getId();
    }

    // zobacz wszystkie watki uzytkownika
    public function showUserSupport(int $ID_author){
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM supp_watek
            WHERE ID_author = :ID_author
            ');
        $stmt->bindParam(':ID_author', $ID_author, PDO::PARAM_INT);
        $stmt->execute();

        $post = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($post == false){
            return null;
        }

        foreach ($post as $one) {
            $result[] = new Support(
                $one['ID_watek'],
                $one['status'],
                $one['ID_author'],
                $one['date'],
                $one['title']
            );
        }
        return $result;
    } 

    //Pobierz wiadomosci danego watku
    public function showMessages(int $ID_watek) :array{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM supp_message
            WHERE ID_watek = :ID_watek
            ORDER BY ID_message
            ');
        $stmt->bindParam(':ID_watek', $ID_watek, PDO::PARAM_INT);
        $stmt->execute();

        $post = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($post == false){
            return null;
        }

        foreach ($post as $one) {
            $result[] = new SuppMess(
                $one['ID_message'],
                $one['ID_watek'],
                $one['who'],
                $one['date'],
                $one['content'],
            );
        }
        return $result;
    }

    // zmiana statusu watku
    public function changeStatus(int $ID_watek, string $status){
        $stmt = $this->database->connect()->prepare('
                UPDATE supp_watek SET status = :status
                WHERE supp_watek.ID_watek = :ID_watek
            ');
        $stmt->bindParam(':ID_watek', $ID_watek, PDO::PARAM_INT);
        $stmt->bindParam(':status', $status, PDO::PARAM_STR);
        $stmt->execute();
    }

    //pobranie watku o podanym ID
    public function takeWatek(int $ID_watek){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM supp_watek
            WHERE ID_watek = :ID_watek
            ');
        $stmt->bindParam(':ID_watek', $ID_watek, PDO::PARAM_INT);
        $stmt->execute();

        $one = $stmt->fetch(PDO::FETCH_ASSOC);

        if(!$one){
            return NULL;
        }

        $result = new Support(
            $one['ID_watek'],
            $one['status'],
            $one['ID_author'],
            $one['date'],
            $one['title']
        );

        return $result;
    }

    //pobranie wszystkich watkow
    public function showAllSupport(){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM supp_watek
            ORDER BY ID_watek 
            ');
        $stmt->execute();

        $post = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($post == false){
            return null;
        }

        foreach ($post as $one) {
            $result[] = new Support(
                $one['ID_watek'],
                $one['status'],
                $one['ID_author'],
                $one['date'],
                $one['title'],
            );
        }
        return $result;
    }

}