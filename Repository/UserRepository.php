<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//Profile.php';

class UserRepository extends Repository {

    public function getUserByLogin(string $login): ?User {

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE login = :login
        ');

        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['ID_user'],
            $user['login'],
            $user['password'],
            $user['email'],
            $user['phone'],
            $user['points'],
            $user['role']
        );
    }

    public function getUserById(string $id): ?User {

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE ID_user = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['ID_user'],
            $user['login'],
            $user['password'],
            $user['email'],
            $user['phone'],
            $user['points'],
            $user['role']
        );
    }

    public function getUserByEmail(string $email): ?User {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE email = :email
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['ID_user'],
            $user['login'],
            $user['password'],
            $user['email'],
            $user['phone'],
            $user['points'],
            $user['role']
        );
    }

    public function makeUser(string $login, string $password, string $email, string $phone){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  USER ( login, password, email, phone) 
            VALUES (:login, :password, :email, :phone)
            ');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
        $stmt->execute();
    }  

    public function makeProfile(int $ID_user, string $description){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  PROFILE ( ID_user, description) 
            VALUES (:ID_user, :description)
            ');
        $stmt->bindParam(':ID_user', $ID_user, PDO::PARAM_INT);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->execute();
    }  

    public function changePassword(string $login, string $password){
        $stmt = $this->database->connect()->prepare('
            UPDATE user SET `password` = :password WHERE `login` = :login
            ');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->execute();

    }

    public function changeEmail(string $login, string $newEmail){
        $stmt = $this->database->connect()->prepare('
            UPDATE user SET `email` = :newEmail WHERE `login` = :login
            ');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':newEmail', $newEmail, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function changePhone(string $login, string $newPhone){
        $stmt = $this->database->connect()->prepare('
            UPDATE user SET `phone` = :newPhone WHERE `login` = :login
            ');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':newPhone', $newPhone, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function showProfile(int $ID_user){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM profile
            WHERE ID_user = :ID_user
        ');
        $stmt->bindParam(':ID_user', $ID_user, PDO::PARAM_STR);
        $stmt->execute();

        $profile = $stmt->fetch(PDO::FETCH_ASSOC);

        if($profile == false) {
            return null;
        }

        return new Profile(
            $profile['ID_profile'],
            $profile['ID_user'],
            $profile['description']
        );
    }

    public function changeProfile(int $ID_user, string $description){
        $stmt = $this->database->connect()->prepare('
            UPDATE profile SET description = :description 
            WHERE ID_user = :ID_user
            ');
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->bindParam(':ID_user', $ID_user, PDO::PARAM_INT);
        $stmt->execute();
    }

}