<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//News.php';
require_once __DIR__.'//..//Models//User.php';

class NewsRepository extends Repository {

    //pobierz 5 newsow
    public function getNews(){
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM news 
            ORDER BY ID_news desc
            LIMIT 4
        ');
        $stmt->execute();
        $news = $stmt->fetchAll(PDO::FETCH_ASSOC);


        if($news == false){
            return null;
        }

        foreach ($news as $one) {
            $result[] = new News(
                $one['ID_news'],
                $one['time'],
                $one['date'],
                $one['content'],
                $one['author']
            );
        }
        return $result;
    }
    
    //dodaj news
    public function makeNews(string $time, string $date, string $content, string $author){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO NEWS ( time, date, content, author) 
            VALUES (:time, :date, :content, :author)
            ');

        $stmt->bindParam(':time', $time, PDO::PARAM_STR);
        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->bindParam(':content', $content, PDO::PARAM_STR);
        $stmt->bindParam(':author', $author, PDO::PARAM_STR);
        $stmt->execute();
    }  
    //usun news
    public function deleteNews(int $id){
        $stmt = $this->database->connect()->prepare('
            DELETE FROM `news` WHERE `news`.`ID_news` = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function showRanking() :array{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user
            WHERE role like "user"
            ORDER BY points desc
        ');
        $stmt->execute();
        $ranking = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];

        if($ranking == false){
            return null;
        }

        foreach ($ranking as $one) {
            $result[] = new User(
                $one['ID_user'],
                $one['login'],
                $one['password'],
                $one['email'],
                $one['phone'],
                $one['points'],
                $one['role']
            );
        }
        return $result;
    }
}