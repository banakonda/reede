let i = 0;

$('.reg').click(function(){
    if(i == 0){
        $('.login').fadeOut(200, function(){
            $('.register').fadeIn();
        });
        i = 1;
    } else {
        $('.forgot').fadeOut(200, function(){
            $('.register').fadeIn();
        });
        i = 1;
    }
    
});

$('.log').click(function(){
    if(i == 1){
        $('.register').fadeOut(300, function(){
            $('.login').fadeIn();
        });
        i = 0;
    } else {
        $('.forgot').fadeOut(300, function(){
            $('.login').fadeIn();
        });
        i = 0;
    }
});

$('.for').click(function(){
    if(i == 0){
        $('.login').fadeOut(300, function(){
            $('.forgot').fadeIn();
        });
        i = 2;
    } else {
        $('.register').fadeOut(300, function(){
            $('.forgot').fadeIn();
        });
        i = 2;
    }
});

function validateLogin(x){
    var y = x.value;
    var ll = y.length;

    if(ll < 6){
        $('.mess').html('Login jest zbyt krótki (minimum 6 znaków)!');
        $('.mess').fadeIn('200');
    }
    else{
        $('.mess').fadeOut('200', function(){
            $('.mess').html('');
        }); 
    }
}

function validateEmail(x){
    var y = x.value;

    if(y.indexOf('@') < 0){
        $('.mess').html('Email musi zawierać znak "@"!');
        $('.mess').fadeIn('200');
    }
    else{
        $('.mess').fadeOut('200', function(){
            $('.mess').html('');
        });    
    }
}

function validatePass(x){
    var y = x.value;
    var ll = y.length;
    if(ll < 6){
        $('.mess').html('Hasło jest zbyt krótkie!');
        $('.mess').fadeIn('200');
    }
    else{
        $('.mess').fadeOut('200', function(){
            $('.mess').html('');
        });    
    }
}

function validatePhone(x){
    var y = x.value;
    var ll = y.length;
    if(ll < 9){
        $('.mess').html('Numer telefonu musi mieć conajmniej 9 cyfr!');
        $('.mess').fadeIn('200');
    }
    else{
        $('.mess').fadeOut('200', function(){
            $('.mess').html('');
        });    
    }
}