Projekt aplikacji podobnej do WattPada, z tym że:
-> Użytkownik może udostępnić książke do edycji ogólnej
-> Użytkownik może dać status 'otwarty na pomoc'
   - każdy może napisać propozycje rozdzialu
   - propozycje laduja do zakladki, gdzie autor je odrzuca/akceptuje
   - ksiazka zamknieta, pisana tylko przez autora
  Publiczność książki/artykułu można zawsze zmienić.