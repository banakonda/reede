<?php

require_once 'Controllers//StartController.php';
require_once 'Controllers//BoardController.php';
require_once 'Controllers//SettingsController.php';
require_once 'Controllers//HelpController.php';
require_once 'Controllers//AdminController.php';
require_once 'Controllers//GameController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'start' => [
                'controller' => 'StartController',
                'action' => 'start'
            ],
            'register' => [
                'controller' => 'StartController',
                'action' => 'register'
            ],
            'forgot' => [
                'controller' => 'StartController',
                'action' => 'forgot'
            ],
            'login' => [
                'controller' => 'StartController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'StartController',
                'action' => 'logout'
            ],
            'news' => [
                'controller' => 'BoardController',
                'action' => 'news'
            ],
            'addNews' => [
                'controller' => 'BoardController',
                'action' => 'addNews'
            ],
            'deleteNews' => [
                'controller' => 'BoardController',
                'action' => 'deleteNews'
            ],
            'settings' => [
                'controller' => 'SettingsController',
                'action' => 'settings'
            ],
            'newEmail' => [
                'controller' => 'SettingsController',
                'action' => 'newEmail'
            ],
            'newPhone' => [
                'controller' => 'SettingsController',
                'action' => 'newPhone'
            ],
            'newPass' => [
                'controller' => 'SettingsController',
                'action' => 'newPass'
            ],
            'help' => [
                'controller' => 'HelpController',
                'action' => 'help'
            ],
            'support' => [
                'controller' => 'HelpController',
                'action' => 'support'
            ],
            'makeWatek' => [
                'controller' => 'HelpController',
                'action' => 'makeWatek'
            ],
            'readWatek' => [
                'controller' => 'HelpController',
                'action' => 'readWatek'
            ],
            'sendMess' => [
                'controller' => 'HelpController',
                'action' => 'sendMess'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'admin'
            ],
            'adminSupport' => [
                'controller' => 'AdminController',
                'action' => 'adminSupport'
            ],
            'profile' => [
                'controller' => 'SettingsController',
                'action' => 'profile'
            ],
            'editProfile' => [
                'controller' => 'SettingsController',
                'action' => 'editProfile'
            ],
            'ranking'  => [
                'controller' => 'BoardController',
                'action' => 'ranking'
            ],
            'search'  => [
                'controller' => 'GameController',
                'action' => 'search'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'start';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}