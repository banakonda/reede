<?php
// Wiadomosci watek
//ID_message	ID_watek	who	date	content

class SuppMess {
    private $ID_message;
    private $ID_watek;
    private $who;
    private $date;
    private $title;


    public function __construct(
        int $ID_message = null,
        int $ID_watek,
        string $who,
        string $date,
        string $content
    ) {
        $this->ID_watek = $ID_watek;
        $this->who = $who;
        $this->date = $date;
        $this->content = $content;
    }

    public function getId() :int {
        return $this->ID_message;
    }
    public function getIdWatek() :int {
        return $this->ID_watek;
    }
    public function getWho() :string{
        return $this->who;
    }
    public function getDate() :string{
        return $this->date;
    }
    public function getContent() :string{
        return $this->content;
    }
}