<?php

class News {
    private $ID_news;
    private $time;
    private $date;
    private $content;
    private $author;


    public function __construct(
        int $ID_news = null,
        string $time,
        string $date,
        string $content,
        string $author
    ) {
        $this->time = $time;
        $this->date = $date;
        $this->content = $content;
        $this->author = $author;
        $this->ID_news = $ID_news;
    }

    public function getId() :int {
        return $this->ID_news;
    }
    public function getTime() :string {
        return $this->time;
    }
    public function getDate() :string{
        return $this->date;
    }
    public function getContent() :string{
        return $this->content;
    }
    public function getAuthor() :string{
        return $this->author;
    }
}