<?php
// Tematy / Watki

class Support {
    private $ID_watek;
    private $status;
    private $ID_author;
    private $date;
    private $title;


    public function __construct(
        int $ID_watek = null,
        string $status,
        int $ID_author,
        string $date,
        string $title
    ) {
        $this->status = $status;
        $this->ID_author = $ID_author;
        $this->date = $date;
        $this->title = $title;
        $this->ID_watek = $ID_watek;
    }

    public function getId() :int {
        return $this->ID_watek;
    }
    public function getStatus() :string {
        return $this->status;
    }
    public function getAuthor() :int{
        return $this->ID_author;
    }
    public function getDate() :string{
        return $this->date;
    }
    public function getTitle() :string{
        return $this->title;
    }
}