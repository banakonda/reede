<?php

class User {
    private $ID_user;
    private $login;
    private $password;
    private $email;
    private $phone;
    private $points;
    private $role;

    public function __construct(
        int $ID_user = null,
        string $login,
        string $password,
        string $email,
        string $phone,
        int $points = 0,
        string $role = 'user'
    ) {
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
        $this->phone = $phone;
        $this->points = $points;
        $this->role = $role;
        $this->ID_user = $ID_user;
    }

    public function getId() :int {
        return $this->ID_user;
    }
    public function getLogin() :string {
        return $this->login;
    }
    public function getPassword() :string{
        return $this->password;
    }
    public function getEmail() :string{
        return $this->email;
    }
    public function getPhone() :string{
        return $this->phone;
    }
    public function getPoints() :int{
        return $this->points;
    }
    public function getRole() :string{
        return $this->role;
    }
}