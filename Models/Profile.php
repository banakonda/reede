<?php
// ID_profile   ID_user   description

class Profile {
    private $ID_profile;
    private $ID_user;
    private $description;


    public function __construct(
        int $ID_profile = null,
        int $ID_user,
        string $description
    ) {
        $this->ID_user = $ID_user;
        $this->description = $description;
        $this->ID_profile = $ID_profile;
    }

    public function getId() :int {
        return $this->ID_profile;
    }
    public function getIdUser() :int {
        return $this->ID_user;
    }
    public function getDescription() :string{
        return $this->description;
    }
}