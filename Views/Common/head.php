<!DOCTYPE HTML>
<html lang='pl'>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Reedre</title>     

        <link rel="Stylesheet" type="text/css" href="Public/css/app.css" />
        <?php
            if($_SESSION){
                if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
                    echo('<link rel="Stylesheet" type="text/css" href="Public/css/admin.css" />');
                }
            }
        ?>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <script src='Public/js/app.js' async></script>
