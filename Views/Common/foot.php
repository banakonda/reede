
        <section class='nav'>
            <section class='who'>
            
                <h2>
                    <?php
                        if(!$_SESSION){
                            echo('Gość');
                        } else {
                            echo($_SESSION['login']);
                        }
                    ?>
                </h2>
                <p class='small'>
                    <?php
                        if($_SESSION){
                            echo('Points: '.$_SESSION['points']);
                        }
                    ?>
                </p>

            </section>

            <section class='options'>
                <a href='?page=news'>Aktualności</a>
                <a href='?page=ranking'>Ranking użytkowników</a>

                <?php
                    if($_SESSION){
                        echo("<a href='?page=search'>Szukaj gier X</a>");
                        echo("<a href='?page=profile&id=".$_SESSION['id']."'>Profil</a>");
                        echo("<a href='?page=settings'>Ustawienia</a>");
                        if($_SESSION['role'] == 'mod' || $_SESSION['role'] == 'admin'){
                            echo("<a href='https://trello.com/b/jkTHK8Zl/reede-albo-inna-nazwa' class='admin-nav target='_blank'>Tablica zadań</a>");
                            echo("<a href='?page=admin' class='admin-nav'>Panel admina O</a>");
                        }
                        if($_SESSION['role'] == 'admin'){
                            echo("<a href='https://localhost/phpmyadmin' class='admin-nav target='_blank'>Database</a>");
                        }
                    }
                ?>
                <a href='?page=help'>Pomoc</a>
                <?php if($_SESSION){ ?>
                    <a href='?page=logout' class='log'>Wyloguj</a>
                <?php }
                else { ?>
                    <a href='?page=start' class='log'>Załóż konto</a>
                <?php }?>
                
            </section>

            <p class='down'>
                Wszelkie prawa zastrzeżone | 2020 &copy | v. 0.4.3
            </p>
        </section>
    </body>
</html>