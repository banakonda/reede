<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<link rel="Stylesheet" type="text/css" href="Public/css/news.css" />
</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Aktualności</h1> 

    </div>



    <section class='content'>
    
    <?php foreach ($news as $item): ?>
        <article class='news'>

            <div class='square'>
                <p><?=$item->getTime() ?></p>
                <p><?=$item->getDate() ?></p>
            </div>
            <section class='news-content'>
                <p>
                <?=$item->getContent() ?>
                </p>
                <p class='author'>
                <?=$item->getAuthor() ?>
                </p>
            </section>
            <?php 
                if($_SESSION){
                    if($_SESSION['role'] == 'admin'){
                        echo("<div class='news-tools'>");
                            echo("
                                <form method='POST' action='?page=deleteNews'>
                                <input name='which' value='
                            ");
                            echo($item->getId());
                            echo("' 
                                    style='display:none'>
                                    <input type='submit' value='X'>
                                </form>
                            ");
                        echo("</div>");
                    }
                }
            ?>
        </article>
    <?php endforeach ?>

    <?php 
        if($_SESSION){
            if($_SESSION['role'] == 'admin'){
                echo("<div class='news'>");
                echo("<form action='?page=addNews' method='POST'>");
                echo("<textarea name='content' placeholder='Wprowadź treść newsa.'></textarea>");
                echo("<input type='submit'>");
                echo("</form>");
                echo("</div>");
            }
        }
        ?>
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
