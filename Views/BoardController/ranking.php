<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<!-- miejsce na dodatkowe .cssy lub dołączenie jquery, itd -->
<link rel="Stylesheet" type="text/css" href="Public/css/ranking.css" />
<!-- koniec tego miejsca -->

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Ranking</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac content -->

            <div class='table'>
                <?php 
                $i = 1;
                foreach($rank as $one): ?>
                <div class="line">
                    <div class="place"><p><?=$i?>.</p></div>
                    <div class="nick"><p><?=$one->getLogin()?></p></div>
                    <div class="points"><p><?=$one->getPoints()?></p></div>
                    <div class='profile'><a href='?page=profile&id=<?= $one->getId()?>'>Profil</a></div>
                </div>
                <?php 
                $i++;
                endforeach?>

            </div>

            <p class='inf'>
                Osoby z najwyższych miejsc mają realny wpływ na rozwój strony oraz mogą starać się o rangę moderatora. W tym celu należy napisać podanie przez panel kontaktowy (zakładka: Pomoc -> napisz wiadomość).
            </p>
        <!-- do tego miejsca content -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
