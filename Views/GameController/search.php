<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<!-- miejsce na dodatkowe .cssy lub dołączenie jquery, itd -->
<link rel="Stylesheet" type="text/css" href="Public/css/search.css" />
<!-- koniec tego miejsca -->

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Title</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac content -->

        <div class='option'>
            <h2>Szukaj gier:</h2>
            <form method='POST' action='?page=search'>
                <input type='text' name='name' placeholder='Szukaj!'>
                <input type='submit' value='Szukaj!'>
            <form>
        </div>


        <div class='option'>
            <h2>Polecane gry:</h2>
        
            <div class='recomend'>
                <div class='game'>
                    <h3>Title.php</h3>
                    <img src='Public/img/1.png' alt='obrazek'>
                    <p class='opis'>Dynamiczna gra MMORPG, w której możesz grać 6 postaciami.</p>
                </div>
                <div class='game'>
                    <h3>Title.php</h3>
                    <img src='Public/img/1.png' alt='obrazek'>
                    <p class='opis'>Dynamiczna gra MMORPG, w której możesz grać 6 postaciami.</p>
                </div>
                <div class='game'>
                    <h3>Title.php</h3>
                    <img src='Public/img/1.png' alt='obrazek'>
                    <p class='opis'>Dynamiczna gra MMORPG, w której możesz grać 6 postaciami.</p>
                </div>
                <div class='game'>
                    <h3>Title.php</h3>
                    <img src='Public/img/1.png' alt='obrazek'>
                    <p class='opis'>Dynamiczna gra MMORPG, w której możesz grać 6 postaciami.</p>
                </div>
            </div>
        </div>
        <!-- do tego miejsca content -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
