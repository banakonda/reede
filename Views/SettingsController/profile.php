<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<!-- miejsce na dodatkowe .cssy lub dołączenie jquery, itd -->
<link rel="Stylesheet" type="text/css" href="Public/css/profile.css" />
<!-- koniec tego miejsca -->

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Profil</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac content -->

            <div class='table'>
                <div class='line'>
                    <div class='nick dark'>
                        <p><?= $this->gamer->getLogin(); ?></p>
                    </div>
                    <div class='field'>
                        <p>
                            <?php
                                $rola = $this->gamer->getRole();
                                if($rola == 'user')
                                    echo('Użytkownik');
                                else if($rola == 'mod')
                                    echo('Moderator');
                                else if($rola == 'admin')
                                    echo('Administrator');
                                else 
                                    echo('Nieznana ranga');
                            ?>
                    </p>
                    </div>
                    <div class='field'>
                        <p>Points: <?= $this->gamer->getPoints(); ?></p>
                    </div>
                </div>
                
                <div class='line cont'>
                    <p><?= $this->profile->getDescription(); ?></p>
                </div>
            </div>

            <?php
                if($_SESSION){
                    if($_SESSION['id'] == $_GET['id'] || $_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
                        echo('<a href="?page=editProfile&id='.$_GET['id'].'" class="butt">Edytuj profil</a>');
                    }
                }
            ?>

        <!-- do tego miejsca content -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
