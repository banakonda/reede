<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<!-- miejsce na dodatkowe .cssy lub dołączenie jquery, itd -->
<link rel="Stylesheet" type="text/css" href="Public/css/profile.css" />
<!-- koniec tego miejsca -->

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Edycja profilu</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac content -->
            <a href='?page=profile&id=<?=$_GET["id"]?>' class='butt'>Powrót</a>

            <form method='POST' action='?page=editProfile&id=<?=$_GET["id"]?>'>
                <textarea name='newDesc'><?=$this->profile->getDescription()?></textarea>
                <input type='submit' value='Zapisz'>
            </form>

            <div class='instruction'>
                <br>
                Instrukcja:
                <br><br>
                [br] - przejście do nowej*<br>
                [b] - pogrubienie*<br>
                [i] - kursywa*<br>
                [u] - podkreślenie*
                <br><br>
                *zamiast [] należy stosować <>
            </div>
        <!-- do tego miejsca content -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
