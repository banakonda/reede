<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<link rel="Stylesheet" type="text/css" href="Public/css/settings.css" />
</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Ustawienia</h1> 
    </div>

    <section class='content'>
        
            <div class='option'>
                <div class='ractangle'>
                    E-mail
                </div>
                <div class='cont'>
                    <p>Aktualny email: <span class='actual'><?=$user->getEmail()?></span></p>
                    <form method='POST' action='?page=newEmail'>
                        <input name='newEmail' type='text' placeholder='Nowy mail'>
                        <input type='submit'>
                    </form>
                    <p class='inf'>Pamiętaj, że email musi posiadać znak @! </p>
                </div>
            </div>

            <div class='option'>
                <div class='ractangle'>
                    Nr. telefonu
                </div>
                <div class='cont'>
                    <p>Aktualny numer telefonu: <span class='actual'><?=$user->getPhone()?></span></p>
                    <form method='POST' action='?page=newPhone'>
                        <input name='newPhone' type='text' placeholder='Nowy nr telefonu'>
                        <input type='submit'>
                    </form>
                    <p class='inf'>Pamiętaj, że nr telefonu musi mieć conajmniej 9 cyfr! </p>
                </div>
            </div>

            <div class='option'>
                <div class='ractangle'>
                    Hasło
                </div>
                <div class='cont'>
                    <p>Możliwość zmiany hasła:</p>
                    <form method='POST' action='?page=newPass'>
                        <input name='oldPass' type='password' placeholder='Stare hasło'>
                        <input name='newPass1' type='password' placeholder='Nowe hasło'>
                        <input name='newPass2' type='password' placeholder='Nowe hasło'>
                        <input type='submit'>
                    </form>
                    <p class='inf'>Pamiętaj, że hasło musi mieć conajmniej 6 znaków! </p>
                </div>
            </div>
        
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
