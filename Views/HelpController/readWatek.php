<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<link rel="Stylesheet" type="text/css" href="Public/css/suppMess.css" />

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Watek</h1>
        <h3><?php echo($this->title); ?></h3> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac -->
        <?php
            if($_SESSION)
                if($_SESSION['role']=='admin' || $_SESSION['role']=='mod')
                    echo("<a href='?page=adminSupport' class='back'>Wróć</a>");
                else
                    echo("<a href='?page=support' class='back'>Wróć</a>");
        ?>
            <?php foreach ($messages as $post): ?>
                <article class='box'>
                    <section class='ractangle'>
                        <p><?= $post->getWho()?> <p>(<?= $post->getDate()?>)</p></p>
                    </section>

                    <section class='cont'>
                        <p><?= $post->getContent() ?></p>
                    </section>
                </article>

            <?php endforeach ?> 

            <?php
                if($_SESSION){
                    if($_SESSION['role']=='admin')
                        echo('<p style="color: #cc4444">Jesteś zalogowany jako administrator!</p>');
                    if($_SESSION['role']=='mod')
                        echo('<p style="color: #cc4444">Jesteś zalogowany jako moderatotr!</p>');
                }
            ?>

            <section class='supp'>
                <form method='POST' action=<?php echo('?page=sendMess&nr='. $_GET['nr']); ?>>
                    <textarea name='content' type='text' placeholder='Treść wiadomości.'></textarea>
                    <div class='inputs'>
                        <input type='submit' name='Send' value='Wyślij!'>
                        <input type='submit' name='SendClose' value='Wyślij i zamknij!'>
                        <input type='submit' name='Close' value='Zamknij!'>
                    </div>
                </form>
            </section>

        <!-- do tego miejsca -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
