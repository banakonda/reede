<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<link rel="Stylesheet" type="text/css" href="Public/css/help.css" />

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Pomoc</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac -->
            <ol>
                <li>
                    <p>Jest to wersja alfa aplikacji. Mogą w niej występować błędy. Wszystkie proszę o zgłaszanie do administratora poprzez formularz poniżej (należy być zalogowanym). Za pomoc w znalezieniu błędów przewidywane są nagrody w postaci punktów.</p>
                </li>
                <li>
                    <p>W razie wystąpienia błędów z przesłaniem wiadomości, proszę o kontakt poprzez mail: ft1.tomasz@gmail.com.</p>
                </li>
                <li>
                    <p>W przypadku niezgłoszenia błędów będzie nakładana blokada konta.</p>
                </li>
                <li>
                    <p>Do czasu przejścia na wersję beta lub pełną, administracja zastrzega sobie prawo do usunięcia wpisów użytkowników, jeśli jest ku temu powód. Użytkownik będzie o takim fakcie informowany.</p>
                </li>
            </ol>

            <?php
            if($_SESSION){
                if($_SESSION['role'] == 'user')
                    echo("
                    <br><a href='?page=support' class='supp-butt'>Napisz wiadomość</a>");
            }
            ?>
        <!-- do tego miejsca -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
