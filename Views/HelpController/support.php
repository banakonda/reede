<?php include(dirname(__DIR__).'../Common/head.php'); ?>
<link rel="Stylesheet" type="text/css" href="Public/css/support.css" />

</head>
<body>

<main class='container'>
    <div class='title'>
        <h1>Support</h1> 
    </div>

    <section class='content'>
        <!-- stad mozesz pisac -->

        <p> Witaj na panelu kontaktowym. Oto Twoje otwarte wątki: </p>
        <?php if($watki) foreach ($watki as $watek): ?>
            <?php if($watek->getStatus() == 'open'){
                    $link = '?page=readWatek&nr=';
                    $link .= $watek->getId();
                ?>
                <a href=<?php echo($link);?>><article class='watek'>
                    <h2><?=$watek->getTitle()?></h2>
                    <p><?=$watek->getDate()?></p>
                </article></a>

            <?php }
            else if($watek->getStatus() == 'answer'){
                $link = '?page=readWatek&nr=';
                $link .= $watek->getId();
            ?>
            <a href=<?php echo($link);?>><article class='watekG'>
                <h2>(Nowa wiadomość) <?=$watek->getTitle()?></h2>
                <p><?=$watek->getDate()?></p>
            </article></a>

        <?php }
             endforeach; 
        
        else 
        echo('<p style="color: #cc4444">-- Brak otwartych watkow --</p>')?>

        <p>Możesz również napisać wiadomość: </p>
            <?php
            if($_SESSION){
                    echo("
                    <section class='supp'>

                        <form method='POST' action='?page=makeWatek'>
                            <input name='title' type='text' placeholder='Temat wiadomości.'>
                            <textarea name='content' type='text' placeholder='Treść wiadomości.'></textarea>
                            <input type='submit' value='Wyślij!'>
                        </form>

                    </section>");
            }
            ?>
        <!-- do tego miejsca -->
    </section>

</main>

<?php include(dirname(__DIR__).'../Common/foot.php'); ?>
