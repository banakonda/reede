<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Reedre</title>      

        <link rel="Stylesheet" type="text/css" href="Public/css/start.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src='Public/js/start.js' async></script>
    </head>

    <body>
        <main class='container'>
            <section class='left'>
                <div class='login'>
                    <h2>Zaloguj się</h2>
                    <div class='mess-box'>
                        <p class='messG'>
                            <?php
                                if(isset($messagesG)){
                                    foreach($messagesG as $message) {
                                        echo $message;
                                    }
                                }
                            ?>
                        </p>
                        <p class='messR'>
                            <?php
                                if(isset($messagesR)){
                                    foreach($messagesR as $message) {
                                        echo $message;
                                    }
                                }
                            ?>
                        </p>
                    </div>

                    <form action='?page=login' method="POST">
                        <input name='login' type='text' placeholder='Login'>
                        <input name='password' type='password' placeholder='●●●●●●●●'>
                        <button>-></button>
                    </form>

                    <p class='log-else'>
                        <span class='reg'>Nie mam konta</span> | <span class='for'>Nie pamiętam hasła</span>
                    </p>
                </div>

                <div class='register'>
                    <h2>Zarejestruj się</h2>
                    <div class='mess-box'>
                        <p class='mess'></p>
                    </div>
                    <form action='?page=register' method="POST">
                        <input name='login' type='text' placeholder='Login' onChange="validateLogin(this);">
                        <input name='email' type='text' placeholder='Email' onChange="validateEmail(this);">
                        <input name='password1' type='password' placeholder='●●●●●●●●' onChange="validatePass(this);">
                        <input name='password2' type='password' placeholder='●●●●●●●●'>
                        <input name='phone' type='text' placeholder='697876132' onChange="validatePhone(this);">
                        <input type='submit' value='->'>
                    </form>

                    <p class='log-else'>
                        <span class='log'>Mam już konto</span> | <span class='for'>Nie pamiętam hasła</span>
                    </p>
                </div>

                <div class='forgot'>
                    <h2>Odzyskaj hasło</h2>
                        <form action='?page=forgot' method="POST">
                            <input name='login' type='text' placeholder='Login'>
                            <input name='email' type='text' placeholder='Email'>
                            <input name='phone' type='text' placeholder='697876132'>
                            <button>-></button>
                        </form>

                        <p class='log-else'>
                            <span class='log'>Mam już konto</span> | <span class='reg'>Nie mam konta</span>
                        </p>
                </div>

            </section>

            <section class='intro'>
            
                <h1>Witaj w Reede!</h1>
                <p>Czeka na Ciebie tysiące graczy z całego świata, z którymi będziesz mógł zaplanopwać podbój kolejnych gier!</p>

                <a class='go-next' href='?page=news' type='button'>Przejdź dalej</a>
            </section>
        </main>
    </body>
</html>