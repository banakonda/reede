<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SettingsController extends AppController {
    public function __construct(){
        session_start();
        $this->userRepository = new UserRepository();
        $this->user = $this->userRepository->getUserById($_SESSION['id']);
    }
    
    public function settings(){
        $this->render('settings', ['user' => $this->user]);
    }

    public function newEmail(){
        if ($this->isPost()) {
            $newEmail = $_POST['newEmail'];

            if(strpos($newEmail, '@') != null){
                $this->userRepository->changeEmail($_SESSION['login'], $newEmail);
            }

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=settings");

            return;
        }
        $this->render('settings', ['user' => $this->user]);
    }

    public function newPhone(){
        if ($this->isPost()) {
            $newPhone = $_POST['newPhone'];

            if(strlen($newPhone) > 8){
                $this->userRepository->changePhone($_SESSION['login'], $newPhone);
            }
            
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=settings");

            return;
        }
        $this->render('settings', ['user' => $this->user]);
    }

    public function newPass(){
        if ($this->isPost()) {
            $password1 = $_POST['newPass1'];
            $password2 = $_POST['newPass2'];
            $oldPass = $_POST['oldPass'];


            if($password1 == $password2 && password_verify($oldPass, $this->user->getPassword())){
                $this->user = $this->userRepository->changePassword($_SESSION['login'], password_hash($password1, PASSWORD_DEFAULT));
            }
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=settings");
            return;
        }
        $this->render('settings', ['user' => $this->user]);
    }

    public function profile(){
        $this->gamer = $this->userRepository->getUserById($_GET['id']);
        $this->profile = $this->userRepository->showProfile($_GET['id']);
        $this->render('profile');
    }

    public function editProfile(){
        $id = $_GET['id'];
        if($id != $this->user->getId() && $_SESSION['role'] == 'user'){
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=profile&Profile&id=".$this->user->getId());
            return;
        }
        if($this->isPost()){
            $newDesc = $_POST['newDesc'];
            $this->userRepository->changeProfile($id, $newDesc);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=profile&id=".$id);
            return;
        }
        $this->profile = $this->userRepository->showProfile($id);
        $this->render('editProfile');
    }
}