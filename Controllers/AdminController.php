<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//SupportRepository.php';


class AdminController extends AppController {
    public function __construct(){
        session_start();
        $this->supportRepository = new SupportRepository;
    }
    
    public function admin(){
        if($_SESSION){
            if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
                $this->render('admin');
                return;
            }
        }

        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}/projekt/?page=news");
        return;
    }

    public function adminSupport(){
        if($_SESSION){
            if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
                $watki =  $this->supportRepository->showAllSupport();
                $this->render('adminSupport', ['watki' => $watki]);
                return;
            }
        }
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}/projekt/?page=news");
        return;
    }

}