<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//SupportRepository.php';
// require_once __DIR__.'//..//Repository//UserRepository.php';

class HelpController extends AppController {
    public function __construct(){
        session_start();
        $this->supportRepository = new SupportRepository;
    }

    public function help(){
        $this->render('help');
    }

    public function support(){
        if(!$_SESSION){
            $this->render('help');
            return;
        }
        $status = 'open';
        $watki = $this->supportRepository->showUserSupport($_SESSION['id']);
        $this->render('support', ['watki' => $watki]);
    }

    public function makeWatek(){
        //  stworz watek [ID_watekINT	statusVAR	ID_authorINT	dateVAR 	titleVAR]
        // wyslij wiadomosc [ID_message 	ID_watek	who	    date  content]
        if ($this->isPost()){  
            $status = 'open';
            $ID_author = $_SESSION['id'];
            $date = date('d-m-Y, H:i');
            $title = $_POST['title'];     
            $who = $_SESSION['login'];
            $content = $_POST['content'];

            $this->supportRepository->makeWatek($status, $ID_author, $date, $title);
            $showLastId = $this->supportRepository->lastId();
            $this->supportRepository->sendSuppMess($showLastId, $who, $date, $content);

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=support");
            return;
        }
        $this->render('support');
    }

    public function readWatek(){
        $watek = $this->supportRepository->takeWatek($_GET['nr']);
        if($watek->getAuthor() == $_SESSION['id'] || $_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
            $messages = $this->supportRepository->showMessages($_GET['nr']);
            $this->title = $this->supportRepository->takeWatek($_GET['nr'])->getTitle();
            $this->render('readWatek', ['messages' => $messages]);
            return;
        }
        else{
            $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}/projekt/?page=support");
                return;
        }
    }

    public function sendMess(){

        if($this->isPost()){
            
            if(isset($_POST['Send'])){
                $this->supportRepository->sendSuppMess($_GET['nr'], $_SESSION['login'], date('d-m-Y, H:i'), $_POST['content']); 
                if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'mod'){
                    $this->supportRepository->changeStatus($_GET['nr'], 'answer');
                }  
                else if($_SESSION['role'] == 'user'){
                    $this->supportRepository->changeStatus($_GET['nr'], 'open');
                }  
            }
            else if(isset($_POST['SendClose'])){
                $this->supportRepository->sendSuppMess($_GET['nr'], $_SESSION['login'], date('d-m-Y, H:i'), $_POST['content']);   
                $this->supportRepository->changeStatus($_GET['nr'], 'close');
                
                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}/projekt/?page=support");
                return;
            }
            else if(isset($_POST['Close'])){
                $this->supportRepository->changeStatus($_GET['nr'], 'close');
                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}/projekt/?page=support");
                return;
            }
        }

        $url = "http://$_SERVER[HTTP_HOST]/";
        $uu = '?page=readWatek&nr='.$_GET['nr'];
        header("Location: {$url}/projekt/$uu");
    }


}