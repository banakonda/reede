<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class StartController extends AppController {

    public function __construct(){
        session_start();
        $this->userRepository = new UserRepository();
    }

    public function start(){
        if($_SESSION){
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=news");
            return;
        }
        $this->render('start');
    }    

    public function login(){

        if ($this->isPost()) {
            
            $login = $_POST['login'];
            $password = $_POST['password'];
            $user = $this->userRepository->getUserByLogin($login);
            
            if(!$user) {
                $this->render('start', ['messagesR' => ['Brak użytkownika o podanym loginie!']]);
                return;
            }
            
            if(!password_verify($password, $user->getPassword())){
                $this->render('start', ['messagesR' => ['Nieprawidłowe hasło!']]);
                return;
            }

            $_SESSION["id"] = $user->getId();
            $_SESSION["login"] = $user->getLogin();
            $_SESSION["password"] = $user->getPassword();
            $_SESSION["email"] = $user->getEmail();
            $_SESSION["phone"] = $user->getPhone();
            $_SESSION["points"] = $user->getPoints();
            $_SESSION["role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=news");
            return;
        }

        $this->render('login');
    }

    public function register(){
        if ($this->isPost()) {  
            
            $login = $_POST['login'];
            $email = $_POST['email'];
            $password1 = $_POST['password1'];
            $password2 = $_POST['password2'];
            $phone = $_POST['phone'];

            if(strlen($login) < 6 || strlen($password1) < 6 || strlen($phone) < 9 || strpos($email, '@') == null || $password1 != $password2){
                $this->render('start', ['messagesR' => ['Błąd przy wysyłaniu danych!']]);
                return;
            }

            $user = $this->userRepository->getUserByLogin($login);
            if ($user) {
                $this->render('start', ['messagesR' => ['Użytkownik o takim loginie już istnieje!']]);
                return;
            }

            $user = $this->userRepository->getUserByLogin($email);
            if ($user) {
                $this->render('start', ['messagesR' => ['Użytkownik o takim adresie email już istnieje!']]);
                return;
            }
            
            $hashed_pass = password_hash($password1, PASSWORD_DEFAULT);
            $user = $this->userRepository->makeUser($login, $hashed_pass, $email, $phone);
            $userId = $this->userRepository->getUserByLogin($login); 
            $user = $this->userRepository->makeProfile($userId->getId(), '');
            $this->render('start', ['messagesG' => ['Zarejestrowano pomyślnie!']]);
            return;
        }
        $this->render('start', ['messagesR' => ['Nieoczekiwany błąd!']]);
        return;
    }

    public function forgot(){
        if ($this->isPost()) {
            $login = $_POST['login'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];

            $user = $this->userRepository->getUserByLogin($login);
            if (!$user) {
                $this->render('start', ['messagesR' => ['Brak użytkownika o podanym loginie!']]);
                return;
            }
            
            if($email != $user->getEmail() || $phone != $user->getPhone()){
                $this->render('start', ['messagesR' => ['Nie znaleziono takiego użytkownika!']]);
            }

            $newPassword = '';
            for($i = 0; $i < 8; $i++){
                $newPassword .= strval(rand(0,9));
            }

            $hashed_pass = password_hash($newPassword, PASSWORD_DEFAULT);
            $user = $this->userRepository->changePassword($login, $hashed_pass);

            $mess = 'Twoje nowe hasło to: ';
            $mess .= $newPassword;
            $this->render('start', ['messagesG' => [$mess]]);
        }

    }
    
    public function logout(){
        session_unset();
        session_destroy();

        $this->render('start', ['messagesG' => ['Pomyślnie wylogowano!']]);
    }
}