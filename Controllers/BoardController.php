<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//NewsRepository.php';

class BoardController extends AppController {
    public function __construct(){
        session_start();
        $this->newsRepository = new NewsRepository();
        $this->news = $this->newsRepository->getNews();
    }
    
    public function news(){
        $this->render('news', ['news' => $this->news]);
    }

    public function addNews(){
        if ($this->isPost()){  
            $time = date('H:i');
            $date = date('d-m-Y');
            $content = $_POST['content'];
            $author = $_SESSION['login'];

            $this->newsRepository->makeNews($time, $date, $content, $author);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=news");
            return;
        }
        $this->render('news', ['news' => $this->news]);
    }

    public function deleteNews(){
        if ($this->isPost()){
            
            $id = $_POST['which'];
            $this->newsRepository->deleteNews($id);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/projekt/?page=news");
            return;
        }
        $this->render('news', ['news' => $this->news]);
    }

    public function ranking(){
        $ranking = $this->newsRepository->showRanking();
        $this->render('ranking', ['rank' => $ranking]);
    }
}