<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
//require_once __DIR__.'//..//Repository//GameRepository.php';


class GameController extends AppController {
    public function __construct(){
        session_start();
    }
    
    public function search(){
        $this->render('search');
    }
}